  ---
title: "Your COVID-19 Risk v1: Get European CDC case data"
author: "James Green, of behalf of the Your COVID-19 Risk Team"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (GMT%z)')`"
output:
  html_document:
    self_contained: yes
    always_allow_html: yes
    code_folding: hide
    toc: true
    toc_depth: 2
    keep_md: false
editor_options:
  chunk_output_type: console
---

# Introduction

```{r child = 'your-covid-19-risk-v1-intro.Rmd'}
```

```{r setup}
ufs::checkPkgs(c("knitr",
                 "tidyverse",
                 "httr",
                 "stringr",
                 "readxl",
                 "here"));

source(here::here("v1", "scripts", "your-covid-19-risk-v1-paths.R"));

`%>%` <- dplyr::`%>%`;

idc <- function(x) {
  dplyr::filter(countryNames, geoId == x) %>%
    as.data.frame()
};

idc2 <- function(x) {
  dplyr::filter(totalCases, geoId == x) %>%
    as.data.frame()
};

knitr::opts_chunk$set(echo = TRUE);
```


```{r internet calls}
getDailyCases <- function(path,
                          date = Sys.Date()) {

  ### Create the URL where the dataset is stored
  ### with automatic updates every day
  inputFileName <-
    paste0(format(date, "%Y-%m-%d"), ".xlsx");
  outputFileName <-
    paste0("daily-cases---",
           inputFileName);
  
  url <-
    paste0(
      "https://www.ecdc.europa.eu/sites/default/files/documents/",
      "COVID-19-geographic-disbtribution-worldwide-",
      inputFileName
    );
  
  cat("Will now attempt to download the file from ",
      url,
      "\n", sep="");
  
  #check to see whether data has updated yet (200 = yes, 404 = no)
  status <- httr::HEAD(url)$status_code;
  cat("Status: ", status, "\n", sep="");

  if (status == 404) {
    return(FALSE);
  } else {
    ### Download the dataset from the website to a local temporary file
    httr::GET(
      url,
      httr::authenticate(":", ":", type="ntlm"),
      httr::write_disk(
        file.path(path,
                  outputFileName),
        overwrite = TRUE
      )
    );
    return(invisible(outputFileName));
  }
}

currentDate <- Sys.Date();

dataFileName <-
  getDailyCases(path = dataPath,
                date = currentDate);

if (!file.exists(file.path(dataPath, dataFileName))) {
  if (!dataFileName) {
    ### 404 - try with yesterday
    currentDate <- currentDate - 1;
    dataFileName <-
      getDailyCases(path = dataPath,
                    date = currentDate);
    if (!file.exists(file.path(dataPath, dataFileName))) {
      stop("Can't read data for today or yesterday, either from ",
           "or from locally stored files.");
    }
  }
};

#Find date for most recent JHU date release
jhu_current_date <- Sys.Date()
if(httr::HEAD(paste0("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/", format(jhu_current_date, "%m-%d-%Y"), ".csv"))$status_code == 200) {
  jhu_current_date <- Sys.Date() 
} else {
  jhu_current_date <- Sys.Date() - 1
};
if(httr::HEAD(paste0("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/", format(jhu_current_date, "%m-%d-%Y"), ".csv"))$status_code == 404) {
    jhu_current_date <- Sys.Date() - 2
};

# Pull down last fifteen days of JHU daily data (to calculate fourteen days of difference)
jhu_list <- list();
for (i in 1:15) {
  bob <- readr::read_csv(paste0("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/", format(jhu_current_date - i + 1, "%m-%d-%Y"), ".csv"))
  bob$jhuDate <- jhu_current_date - i + 1
  jhu_list[[i]] <- bob
};
```


```{r data processing}
### Read the Dataset sheet into R

cases <-
  readxl::read_excel(
    file.path(dataPath, dataFileName)
  ) %>%
  dplyr::mutate(id.length = stringr::str_length(geoId)) %>% #remove ships etc
  dplyr::filter(id.length == 2);

### Fix various country codes
cases$geoId[which(cases$geoId == "GQ")] <- "EG";
cases$geoId[which(cases$geoId == "EG")] <- "EY";
cases$geoId[which(cases$geoId == "EL")] <- "GR";
cases$geoId[which(cases$geoId == "XK")] <- "KO";
cases$geoId[which(cases$geoId == "KR")] <- "KS";
cases$geoId[which(cases$geoId == "KN")] <- "KT";
cases$geoId[which(cases$geoId == "MM")] <- "NM";
cases$geoId[which(cases$geoId == "TL")] <- "TP";

#get up-to-date cases

totalCases <- dplyr::group_by(cases, geoId) %>%
  dplyr::summarise (total = sum(cases, na.rm = TRUE), dtotal = sum(deaths, na.rm = TRUE),
             popData2018 = mean(popData2018, na.rm = TRUE));

# get sum of last ten days
rollingCases <- dplyr::group_by(cases, geoId) %>%
  dplyr::filter(dateRep > (as.POSIXct(currentDate) - 86400 * 14)) %>%
  dplyr::summarise (rtotal = sum(cases, na.rm = TRUE), rdtotal = sum(deaths, na.rm = TRUE));

rollingCases2 <- dplyr::group_by(cases, geoId) %>%
  dplyr::filter(dateRep > (as.POSIXct(currentDate) - 86400 * 28) & dateRep < as.POSIXct(currentDate) - 86400 * 13) %>%
  dplyr::summarise (rtotal2 = sum(cases, na.rm = TRUE), rdtotal2 = sum(deaths, na.rm = TRUE));

totalCases <- dplyr::full_join(totalCases, rollingCases, by = "geoId") %>%
  dplyr::full_join(rollingCases2, by = "geoId");

#find countries with missing populations
# nac <- dplyr::filter(currentCases, is.na(popData2018))

#manually enter missing 3 populations
totalCases$popData2018[which(totalCases$geoId == "ER")] <- 3453000;
totalCases$popData2018[which(totalCases$geoId == "BQ")] <- 26000;
totalCases$popData2018[which(totalCases$geoId == "AI")] <- 15000;
totalCases$popData2018[which(totalCases$geoId == "CZ")] <- 10629928;
totalCases$popData2018[which(totalCases$geoId == "FK")] <- 48497;
totalCases$popData2018[which(totalCases$geoId == "EH")] <- 567402; ##Western Sahara not recognised as a country 
                                                                  ##by World Bank so population from Wikipedia

##get hong kong and macao from JHU data
# failed all tidier options (eg bind_rows) so doing it the ugly way :(
jhu_daily <- dplyr::bind_rows(jhu_list[1], jhu_list[2], jhu_list[3], jhu_list[4], jhu_list[5], jhu_list[6], 
                       jhu_list[7], jhu_list[8], jhu_list[9], jhu_list[10], jhu_list[11], jhu_list[12],
                       jhu_list[13], jhu_list[14], jhu_list[15]);

hk.case <- dplyr::filter(jhu_daily, Province_State == "Hong Kong") %>%
  dplyr::mutate(lagc = lag(Confirmed, 1), daily = lagc - Confirmed) %>%
  dplyr::summarise(total = max(Confirmed, na.rm = TRUE), rtotal = sum(daily, na.rm = TRUE), inc10 = rtotal / total);

mo.case <- dplyr::filter(jhu_daily, Province_State == "Macau") %>%
  dplyr::mutate(lagc = lag(Confirmed, 1), daily = lagc - Confirmed) %>%
  dplyr::summarise(total = max(Confirmed, na.rm = TRUE), rtotal = sum(daily, na.rm = TRUE), inc10 = rtotal / total);

#Add HK and MO data to totalCases
totalCases <- totalCases %>%
  dplyr::add_row(geoId = "HK", popData2018 = 7451000, total = hk.case[["total"]], rtotal = hk.case[["rtotal"]]) %>%
  dplyr::add_row(geoId = "MO", popData2018 = 631636, total = mo.case[["total"]], rtotal = mo.case[["rtotal"]]);



#calculate per capita prevalences

totalCases <- dplyr::mutate(totalCases, case100k = total / popData2018 * 100000,
                       death100k = dtotal / popData2018 * 100000,
                     rcase100k = rtotal / popData2018 * 100000,
                     rdeath100k = rdtotal /popData2018 * 100000,
                     rcase100k2 = rtotal2 / popData2018 * 100000,
                     rdeath100k2 = rdtotal2 /popData2018 * 100000,
                     re = rcase100k/rcase100k2, 
                     red = rdeath100k/rdeath100k2,
                     # wtcase = re * rcase100k,
                     # wtdeath = red * rdeath100k,
                     geoId = tolower(geoId));

#cap Re at 4, and fix infinite and nan 
totalCases$re[which(totalCases$re > 4)] <- 4
totalCases$re[which(is.infinite(totalCases$re))] <- 4
totalCases$re[which(is.nan(totalCases$re))] <- 0

totalCases <- dplyr::mutate(totalCases, wtcase100k = re * rcase100k)

#for 0-4 scale, max out at 200cases per 100k
totalCases$twtcase100k <- totalCases$wtcase100k
totalCases$twtcase100k[which(totalCases$wtcase100k > 200)] <- 200
 
# r = n[(A/P)1/nt - 1]
# rate = 14 * ((rcase100k/rcase100k2)^(1/14) -1)
# rate = 14 * ((300/100)^(1/14) -1)

### deprecating these microstate corrections in favour of threshold model

# #modify high prevalence microstates to reflect surrounding countries
# totalCases$case100k[which(totalCases$geoId == "va")] <- totalCases$case100k[which(totalCases$geoId == "it")];
# totalCases$case100k[which(totalCases$geoId == "sm")] <- totalCases$case100k[which(totalCases$geoId == "it")];
# totalCases$case100k[which(totalCases$geoId == "lu")] <- 
#   max(totalCases$case100k[which(totalCases$geoId == "fr")],
#       totalCases$case100k[which(totalCases$geoId == "de")],
#       totalCases$case100k[which(totalCases$geoId == "be")]);
# totalCases$case100k[which(totalCases$geoId == "ad")] <- 
#   max(totalCases$case100k[which(totalCases$geoId == "es")],
#       totalCases$case100k[which(totalCases$geoId == "fr")]);
# 
# totalCases$rcase100k[which(totalCases$geoId == "va")] <- totalCases$rcase100k[which(totalCases$geoId == "it")];
# totalCases$rcase100k[which(totalCases$geoId == "sm")] <- totalCases$rcase100k[which(totalCases$geoId == "it")];
# totalCases$rcase100k[which(totalCases$geoId == "lu")] <- 
#   max(totalCases$rcase100k[which(totalCases$geoId == "fr")],
#       totalCases$rcase100k[which(totalCases$geoId == "de")],
#       totalCases$rcase100k[which(totalCases$geoId == "be")]);
# totalCases$rcase100k[which(totalCases$geoId == "ad")] <- max(totalCases$rcase100k[which(totalCases$geoId == "es")],
#       totalCases$rcase100k[which(totalCases$geoId == "fr")]);
# 
# #and san marino for deaths
# totalCases$death100k[which(totalCases$geoId == "sm")] <- totalCases$death100k[which(totalCases$geoId == "it")];

#lazily using standard prevalence for hong kong and macao, as cases have dropped to essentially zero
totalCases$twtcase100k[totalCases$geoId == "hk"] <- totalCases$rcase100k[totalCases$geoId == "hk"]
totalCases$twtcase100k[totalCases$geoId == "mo"] <- totalCases$rcase100k[totalCases$geoId == "mo"]

totalCases <- dplyr::mutate(totalCases, prev04 = case100k / max(case100k, na.rm = TRUE) * 4,
                     dprev04 = death100k / max(death100k, na.rm = TRUE) * 4,
                     rprev04 = rcase100k / max(rcase100k, na.rm = TRUE) * 4, 
                     rdprev04 = rdeath100k / max(rdeath100k, na.rm = TRUE) * 4,
                     trprev04 = twtcase100k / 200 * 4);



# #plots to compare
# ggplot(totalCases, aes(prev04, dprev04)) +
#   geom_point() +
#   geom_smooth(method = "lm")
# 
# ggplot(totalCases, aes(rprev04, rdprev04)) +
#   geom_point() +
#   geom_smooth(method = "lm")
# 
#compare all time prevalence versus last fourteen days prevalence
ggplot2::ggplot(totalCases, ggplot2::aes(prev04, rprev04, label = geoId)) +
  ggplot2::geom_smooth(method = "lm") +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("Total Cases") +
  ggplot2::scale_y_continuous("New cases in last fourteen days");
#compare all time prevalence versus last fourteen days prevalence with raw case numbers
ggplot2::ggplot(totalCases, ggplot2::aes(case100k, rcase100k, label = geoId)) +
  ggplot2::geom_smooth(method = "lm") +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("Total Cases") +
  ggplot2::scale_y_continuous("New cases in last fourteen days") +
  ggplot2::geom_hline(yintercept = c(50, 100, 150, 200));
#compare rates across two sets of 14 day prevalence
ggplot2::ggplot(totalCases, ggplot2::aes(rcase100k2, rcase100k, label = geoId)) +
  ggplot2::geom_smooth(method = "lm") +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("New cases in previous fourteen days") +
  ggplot2::scale_y_continuous("New cases in last fourteen days") +
  ggplot2::geom_hline(yintercept = c(50, 100, 150, 200));
#new cases v newcases weightd
ggplot2::ggplot(totalCases, ggplot2::aes(rcase100k, wtcase100k, label = geoId)) +
  ggplot2::geom_abline(slope = 1, intercept = 0) +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("New cases in last fourteen days") +
  ggplot2::scale_y_continuous("Infection Force");
#new cases v newcases weightd
ggplot2::ggplot(totalCases, ggplot2::aes(rcase100k, trprev04, label = geoId)) +
  ggplot2::geom_abline(slope = .02, intercept = 0) +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("New cases in last fourteen days per 100,000 population") +
  ggplot2::scale_y_continuous("Risk estimate (Infection Force)");
#new cases versus Re
ggplot2::ggplot(totalCases, ggplot2::aes(rcase100k, re, label = geoId)) +
  ggplot2::geom_smooth(method = "lm") +
  ggplot2::geom_text()+
  ggplot2::scale_x_continuous("New cases in last fourteen days") +
  ggplot2::scale_y_continuous("Effective Reproduction", limits = c(0,3));

# high <- dplyr::filter(totalCases, rprev04 > 2)
# 
# BlandAltmanLeh::bland.altman.plot(totalCases$prev04, totalCases$dprev04, graph.sys = "ggplot2")

#get countries list
# googledrive::drive_download("https://docs.google.com/spreadsheets/d/1xlSUfJb0UfPFWHY8SALMosZnI1veJ2jY_clRtHMZ8JE/edit#gid=1263920418") 

countryNames <-
  dplyr::as_tibble(
    googlesheets::gs_read(
      googlesheets::gs_key(
        "1VJmmUdWvCagdDQvORVSYuz0RjCotg_fP4is67-N-ERU"
      )
    ),
    stringsAsFactors=FALSE
  );

countryNames$geoId <-
  substr(countryNames$code, 1, 2);

# countryNames <- readxl::read_excel("COVID-19-country-list-for-tool.xlsx") %>%
#   select(1:3) %>%
#   dplyr::mutate(geoId = stringr::str_trunc(Code, 2, "right", ""))
# 
# countryPrev <- left_join(countryNames, totalCases, by = "geoId") %>%
#   select(geoId, prev04) 
# 
# countryPrev$prev04[which(is.na(countryPrev$prev04))] <- 0
#
# save(countryPrev,
#      file = here::here("data",
#                        "countryPrev.Rdata"));

convenienceVector <-
  stats::setNames(
    totalCases$trprev04,
    nm = totalCases$geoId
  );

countryNames$geoRiskEstimate <-
  ifelse(is.na(convenienceVector[countryNames$geoId]),
         0,
         convenienceVector[countryNames$geoId]);

hist(countryNames$geoRiskEstimate);
```


```{r write output}
write.csv(countryNames[, c("code", "geoRiskEstimate")],
          file = file.path(dataPath,
                           "countryPrev.csv"),
          row.names = FALSE)

# discuss <- dplyr::full_join(countryNames, totalCases, by = "geoId") %>%
#   dplyr::filter(geoRiskEstimate > 0.5) %>%
#   dplyr::select(- text_translated, - geoId, -rdtotal, - case100k, - death100k, contains("04")) %>%
#   dplyr::rename(totalcases = total, totaldeaths = dtotal, last14days = rtotal, prev14days = rtotal2,
#          last14per100k = rcase100k, prev14per100k = rcase100k2, reproduction = re,
#          estimated_new_infection = twtcase100k, last14daysdeathper100k = rdeath100k)
# 
# write.csv(discuss, file = "discuss.csv")
```

