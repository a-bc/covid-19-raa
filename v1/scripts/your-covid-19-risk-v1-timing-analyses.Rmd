---
title: "Your COVID-19 Risk v1 timing analyses"
author: "Your COVID-19 Risk Team"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (GMT%z)')`"
output:
  html_document:
    self_contained: yes
    always_allow_html: yes
    code_folding: hide
    toc: true
    toc_depth: 2
    keep_md: false
editor_options:
  chunk_output_type: console
---

# Introduction

```{r child = 'your-covid-19-risk-v1-intro.Rmd'}
```

```{r setup, results="asis"}

###-----------------------------------------------------------------------------
### Packages
###-----------------------------------------------------------------------------

#devtools::install_gitlab('r-packages/ufs');

ufs::checkPkgs(
  c(
    ### Easily finding files
    "here"
  )
);

### Get the development version of the fancy new R package `limonaid`,
### developed by Gjalt-Jorn especially for this awesome project
if ("limonaid" %in% loadedNamespaces())
  unloadNamespace("limonaid");
devtools::install_gitlab('r-packages/limonaid');
#devtools::install_gitlab('r-packages/rosetta');

###-----------------------------------------------------------------------------
### Set package options
###-----------------------------------------------------------------------------

### Prevent knitr::kable from printing NAs
options(knitr.kable.NA = '');

### Set figure numbering
ufs::setFigCapNumbering();

### Always print plots when using knitAndSave
ufs::opts$set(knitAndSave.catPlot = TRUE);

###-----------------------------------------------------------------------------
### Project settings and shared paths
###-----------------------------------------------------------------------------

### Paths
source(here::here("v1", "scripts", "your-covid-19-risk-v1-paths.R"));

```

```{r load-data}

dat <-
  read.table(
    unz(file.path(dataPath, "survey_100101_R_data_file.zip"),
        "survey_100101_R_data_file.csv"),
    header = TRUE,
    sep=",",
    quote="\"",
    dec=".",
    fill=TRUE,
    comment.char="",
    stringsAsFactors = FALSE,
    fileEncoding ="UTF-8-BOM"
  );

scriptBits <-
  limonaid::ls_parse_data_import_script(
    scriptfile = file.path(dataPath,
                           "survey_100199_R_syntax_file.R")
  );

dat <-
  limonaid::ls_apply_script_bits(dat,
                                 scriptBits);

```
