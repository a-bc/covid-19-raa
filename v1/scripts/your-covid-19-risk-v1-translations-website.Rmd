---
title: "Your COVID-19 Risk: Translations Website v1"
author: "Your COVID-19 Risk team"
date: "`r format(Sys.time(), '%Y-%m-%d at %H:%M:%S %Z (UTC%z)')`"
output:
  html_document:
    self_contained: yes
    always_allow_html: yes
    code_folding: hide
    toc: true
    toc_depth: 2
    keep_md: false
editor_options:
  chunk_output_type: console
---

# Introduction

```{r child = 'your-covid-19-risk-v1-intro.Rmd'}
```

This document contains the translations read from the translation spreadsheets (see https://your-risk.com/translation-instructions) and parses them into formats that are useful for importing the translations into the website.

```{r setup, results="asis"}

###-----------------------------------------------------------------------------
### Whether to update from Google Sheets
###-----------------------------------------------------------------------------

readGoogleSheets <- TRUE;

###-----------------------------------------------------------------------------
### Packages
###-----------------------------------------------------------------------------

#devtools::install_gitlab('r-packages/ufs');

ufs::checkPkgs(c("googlesheets",
                 "here",
                 "knitr",
                 "kableExtra",
                 "jsonlite"));

source(here::here("v1", "scripts", "your-covid-19-risk-v1-paths.R"));

### Gjalt-Jorn maps his C:/Sync directory to his B: drive using
### the Windows command SUBST - correct this so as not to confuse Java
translationOutputPath <-
  sub("^B:/",
      "C:/Sync/",
      translationOutputPath);

magicToolCommand <-
  paste0("java -cp ",
         sub("^B:/", "C:/Sync/", here::here("utils", "magic-tool")),
         " ConvertUnicodeChars ");

### Prevent knitr::kable from printing NAs
options(knitr.kable.NA = '');

###-----------------------------------------------------------------------------
### Version configurations
###-----------------------------------------------------------------------------

### Language that has the data (e.g. the columns with countries/regions for
### the external resources etc)
dataLanguage <- "en";

### Languages in current major version (v1)
v1Languages <-
  c("am", "ar", "de", "el", "en", "es", "et", "fa", "fi", "fr", "gu",
    "he", "hu", "hi", "id", "it", "ja", "ko", "nl", "pl", "pt", "pt-BR",
    "ro", "ru", "sl", "tr", "ur", "zh-Hant", "zh-Hans");
v1Languages <- sort(v1Languages);

### Primary language (language website defaults to if there is no match with
### one of the active languages) in production, staging, and testing
primaryLanguage_prod <- "en";
primaryLanguage_stge <- "en";
primaryLanguage_test <- "en";

### Languages in production, staging, and testing.
activeLanguages_prod <- c("am", "ar", "de", "el", "en", "es",
                          "fr", "he", "hu",
                          "id", "it", "ja", "ko",
                          "nl", "pl", "pt", "pt-BR", "ro",
                          "tr", "ur", "zh-Hant", "zh-Hans");

activeLanguages_stge <- c("am", "ar", "de", "el", "en", "es",
                          "fr", "gu", "hi", "he", "hu",
                          "id", "it", "ja", "ko",
                          "nl", "pl", "pt", "pt-BR", "ro",
                          "tr", "ur", "zh-Hant", "zh-Hans");

activeLanguages_test <- v1Languages;

### Potentially override the active languages, which activates all languages
#activeLanguages <- NULL;

### Set LimeSurvey URL for the production and testing versions

lsInstallURL <- "https://estimate.your-covid-19-risk.com/";
#lsInstallURL <- "https://your-covid-19-risk.limequery.org/";

toolURL_prod <- paste0(lsInstallURL, "100121?lang=");
toolURL_stge <- paste0(lsInstallURL, "100122?lang=");
toolURL_test <- paste0(lsInstallURL, "100199?lang=");

###-----------------------------------------------------------------------------
### jason is the JSON object to which we add the JSON stuff for the website
###-----------------------------------------------------------------------------

jason <- list();

### Add primary language
jason$primary_language <- primaryLanguage_prod;
jason$tool_url <- toolURL_prod;

```

# Script configuration

This code configures the script: which spreadsheets to process, which worksheets to ignore, which columns to select, and to which format the data should be written (i.e. for reading into LimeSurvey, see https://manual.limesurvey.org/Adding_answers_or_subquestions, or for reading into i18next, see https://www.i18next.com/overview/getting-started).

```{r config}

taskSpecs <-
  list(
    homepage_text = list(gs_key = "1egPRiY98ofS8_YP0TisBkAsEPXChIc3xr6RFcVQUMtQ",
                         label = "Home page",
                         task_type = "i18n",
                         file_ext = "properties"),
    theory_page_text = list(gs_key = "1vNA9qWiCOYNEFxLN94RLIc3Dt6MfURBrgMxRlor2PAM",
                            label = "Theory page",
                            task_type = "i18n",
                            file_ext = "properties"),
    risk_model_page_text = list(gs_key = "10pqBZBmfPshi5gwa7Ws4uUuBtuIoxl2FO-5praSQFtA",
                                label = "Risk model page",
                                task_type = "i18n",
                                file_ext = "properties"),
    data_page_text = list(gs_key = "12n6w4heNh_7oKdlKIajj1ilqQjCv7I8Suz2IDLWXe8U",
                          label = "Data page",
                          task_type = "i18n",
                          file_ext = "properties"),
    disclaimer_page_text = list(gs_key = "109AwCPEGYNXB2Ep80pnRHOK1nzGzydEMxx4nj1_ql1A",
                                label = "Discl. page",
                                task_type = "i18n",
                                file_ext = "properties"),
        
    results_page = list(gs_key = "18pNmA6vk9fhwU5TTGhVHuPWzyW9nBdLu_J1CJcERZ0c",
                        label = "Results page",
                        task_type = "i18n",
                        file_ext = "properties"),
    intervention = list(gs_key = "1SIpUZZRrkiQEknvVpSNX26SycTkt6rFLiPZHY7yYcoc",
                        label = "Inter-vention",
                        task_type = "i18n",
                        file_ext = "properties"),
    
    distRec = list(gs_key = "1TUBtl3ZkHQPvobLYg-nx9XvxApAte7bSUuT5JJGa2_Q",
                   label = "Dist. Rec.",
                   task_type = "country_i18n",
                   file_ext = "properties"),
    distRef = list(gs_key="11s2hYlwMZLsPLQ9cnuIwipT7Jt4C4bT79JoozyVu1do",
                   label = " Dist. Ref.",
                   task_type="country_i18n",
                   file_ext = "properties"),
    commonCloseContactGreeting = list(gs_key = "1IJLPrlUONJOS5qndkw9XuWQPwzNSbRmL3QSEh751Nzo",
                                      label = "Common Close Contact Greeting",
                                      task_type = "country_i18n",
                                      file_ext = "properties"),
    symptomAction = list(gs_key = "1uLwHSaMHmeZixAJ6uSXLWg2Wqqy5idLhx4aiAi88Fy4",
                         label = "Action if sympt.",
                         task_type = "country_i18n",
                         file_ext = "properties"),
    listSituationsHandWashingRec = list(gs_key = "1vQDUuYx1zzAUP06d_xpzfkVphpIavG4mfqS_-JdA8CI",
                                        label = "The list of common hand washing-prompting situations",
                                        task_type = "country_i18n",
                                        file_ext = "properties"),
    resURLsHwExmpls = list(gs_key = "14RAGWuN6dt-JAWx6e7oxw-ZCVrOqjrTHkUKETvwCh-0",
                           label = "Video of hand-washing technique",
                           task_type = "country_i18n",
                           file_ext = "properties"),
    resURLsTempMnem = list(gs_key = "1M5VcsvjqIdcF_nknyd0rIJjsQMwIpPpkjknDYbbOfFM",
                           label = "Durtn. Mnemnc.",
                           task_type = "country_i18n",
                           file_ext = "properties"),
    resURLsStHmTips = list(gs_key = "14A06pmfJh4FqS1pIx_YxdT1MgcYC_D9tINA1KE9I-dY",
                           label = "Stay home tips",
                           task_type = "country_i18n",
                           file_ext = "properties"),
    resURLsStayHmIn = list(gs_key = "14gM_y-Ae-8EVEmhGMHBXq-ID_IcncWtioFjIPPtCSt4",
                           label = "Stay home init.",
                           task_type = "country_i18n",
                           file_ext = "properties"),
    config = list(gs_key = "1HaSzAVENQJS7KKLU382Vj9_ueKuY4o_n6TbgVCeLHxg",
                  label = "Countries & emails",
                  task_type = "config",
                  file_ext = "json"),
    hotfixes = list(gs_key = "17M1VDG2FA5Agt8WBUC8uA0Akmt_5wSn9dFp8IIAtD9g",
                    label = "Transl. hot fixes",
                    task_type = "hotfixes",
                    file_ext = "json")
  );

translationTasksLogical <-
  unlist(
    lapply(
      taskSpecs,
      function(x) {
        return(grepl("i18n|hotfix", x$task_type));
      }
    )
  );

translationTasks <-
  names(taskSpecs)[translationTasksLogical];

```

# Importing Sheets {.tabset}

## Importing the Google sheets

This code imports the google sheets. The `googlesheets` functions provide some feedback that's hard to disable; it has been placed in the other tabs, organised by task.

```{r read-googlesheets, comment="", results="asis"}

taskInput <-
  lapply(
    names(taskSpecs),
    function(taskName) {

      ufs::cat0("\n\n\n## ", taskName, "\n\n\n");
      
      if (readGoogleSheets) {
        
        gsObject <-
          googlesheets::gs_key(taskSpecs[[taskName]]$gs_key,
                               verbose = FALSE);
        sheets <-
          googlesheets::gs_ws_ls(gsObject);
        
        ### Create workbook to back this up to an .xlsx file
        wb <- openxlsx::createWorkbook(creator = "Your COVID-19 Risk Team",
                                       title = taskName,
                                       subject = NULL,
                                       category = NULL);
      
        res <- list();
        for (currentLanguage in sheets) {
          
          ### Read the worksheet for this language
          res[[currentLanguage]] <-
            as.data.frame(
              googlesheets::gs_read(
              gsObject,
              ws = currentLanguage,
              verbose = FALSE
            ),
            stringsAsFactors = FALSE
          );
          
          ### Create worksheet and add data
          openxlsx::addWorksheet(wb,
                                 sheetName = currentLanguage);
          openxlsx::writeData(wb,
                              sheet = currentLanguage,
                              x = res[[currentLanguage]],
                              startCol = "A",
                              startRow = 1);

        }

        ### Write workbook to disk
        openxlsx::saveWorkbook(
          wb,
          file = file.path(websiteSheetsPath,
                           paste0(taskName, ".xlsx")),
          overwrite = TRUE
        );
        
      } else {
        
        ### Read from the local files.
        ufs::cat0("\n\n\n Reading from local backup '",
                  taskName, ".xlsx'.\n\n\n");
        
        sheets <-
          openxlsx::loadWorkbook(
            file.path(websiteSheetsPath,
                      paste0(taskName, ".xlsx"))
          );
        
        res <- list();
        for (currentLanguage in names(sheets)) {
          res[[currentLanguage]] <-
            openxlsx::readWorkbook(
              sheets,
              sheet = currentLanguage
            );
          
        }

      }

      return(res);
    }
  );

names(taskInput) <-
  names(taskSpecs);

```

# Check translations

Here, the provided translations for each task are checked against the list of languages in v1 of the tool: `r ufs::vecTxt(v1Languages, useQuote="\x60");`.

```{r check-translations, results="asis"}

for (currentTask in translationTasks) {
  
  ufs::cat0("\n\n\n## Task: ", currentTask, "\n\n\n");
  
  taskLanguages <- sort(names(taskInput[[currentTask]]));
  omittedLanguages <- !(v1Languages %in% taskLanguages);
  wrongLanguages <- !(taskLanguages %in% v1Languages);
  
  if (any(omittedLanguages)) {
    ufs::cat0("\n### ERROR: There are v1 languages without a worksheet!\n\n");
    ufs::cat0("\nSpecifically:\n\n");
    cat(paste0("- `", v1Languages[omittedLanguages], "`"), sep="\n");
  }
  
  if (any(wrongLanguages)) {
    ufs::cat0("\n### ERROR: There are worksheets with names that do not represent a v1 language!\n\n");
    ufs::cat0("\nSpecifically:\n\n");
    cat(paste0("- `", taskLanguages[wrongLanguages], "`"), sep="\n");
  }
  
  if (all(!omittedLanguages) && all(!wrongLanguages)) {
    cat("\nAll languages are correct!\n");
  }

}

```

Here, the external resources that have to be selected for the tool users based on the country/region they are in are checked. A country/region code can only correspond to exactly one row in each external resources spreadsheet.

```{r check-external-resources, results="asis"}

i18n_country_tasks <-
  unlist(lapply(taskSpecs, function(x) return(x$task_type == "country_i18n")));
i18n_country_task_names <-
  names(i18n_country_tasks)[i18n_country_tasks];

for (currentTask in names(taskInput)[i18n_country_tasks]) {
  
  ufs::cat0("\n\n\n## Task: ", currentTask, "\n\n\n");
  
  countryRegionCodes <-
    strsplit(gsub("\\s", "", taskInput[[currentTask]][[dataLanguage]][[3]]),
             ",",
             fixed = TRUE);

  codeList <-
    tolower(unlist(countryRegionCodes));
  codeList <- codeList[!is.na(codeList)];
  codesThatAreDuplicate <-
    duplicated(codeList);
  duplicatedCodes <-
    codeList[codesThatAreDuplicate];

  if (any(codesThatAreDuplicate)) {
    ufs::cat0("\n\n### ERROR: There are country/region codes in multiple rows!\n\n");
    ufs::cat0("\nSpecifically:\n\n");
    cat(paste0("- `", duplicatedCodes, "`"), sep="\n");
  } else {
    cat("\nNo country/region codes appear in multiple rows, great!\n");
  }
  
  if ("xx00" %in% codeList) {
    ufs::cat0("\nGood - the generic code is included!\n");
  } else {
    ufs::cat0("\n\n### ERROR: The generic code (`xx00`) is not included!\n\n");
  }

}

```

# Produce output {.tabset}

```{r language-overview, results='asis'}

languagePerTask <-
  lapply(translationTasks,
         function(task)
           return(names(taskInput[[task]]))
         );
names(languagePerTask) <-
  translationTasks;

fullLanguageList <-
  unique(unlist(languagePerTask));

languageDf <- data.frame(stringsAsFactors = FALSE);
for (taskName in names(languagePerTask)) {
  for (lang in fullLanguageList) {
    languageDf[lang, taskName] <-
      ifelse(lang %in% languagePerTask[[taskName]],
             "Yes",
             "");
  }
}

kableExtra::kable_styling(
  knitr::kable(
    languageDf,
    col.names = unlist(lapply(taskSpecs[translationTasksLogical],
                              function(x) return(x$label)))
  )
);

```

This code produces the output, with some minimal logging.

```{r produce-output, comment="", results="asis"}

taskOutput <-
  lapply(
    translationTasks,
    function(taskName) {
      
      res <- list();
      ufs::cat0("\n\n\n## ", taskName, "\n\n\n");
      
      if (taskSpecs[[taskName]]$task_type == "limesurvey") {
        
        ufs::cat0("This is a LimeSurvey task.\n");
        ufs::cat0("Not doing anything now - processing this later.\n");

      } else if (taskSpecs[[taskName]]$task_type == "i18n") {
        
        ufs::cat0("This is an i18n task.\n");

        for (currentLanguage in names(taskInput[[taskName]])) {

          ufs::cat0("Starting to process language ", currentLanguage, ".\n");

          ### Object for this language
          res[[currentLanguage]] <- list();
          
          ### Create dataframe for convenience
          dat <- taskInput[[taskName]][[currentLanguage]];
          
          ### Get keys
          res[[currentLanguage]]$keys <-
            dat$key;

          ### Get English texts
          res[[currentLanguage]]$english <-
            stats::setNames(dat$text_en,
                            nm = dat$key);
          
          res[[currentLanguage]]$translation <-
            stats::setNames(dat$text_translated,
                            nm = dat$key);
          
          ### Manually, temporarily, remove one key
          if ("home_who_is_it_text7" %in% res[[currentLanguage]]$keys) {
            res[[currentLanguage]]$english["home_who_is_it_text7"] <- "";
            res[[currentLanguage]]$translation["home_who_is_it_text7"] <- "";
          }

          res[[currentLanguage]]$mergedOutput <-
            res[[currentLanguage]]$output <-
            paste0(
              paste0(
                res[[currentLanguage]]$keys,
                "=",
                gsub("\n|\r",
                     " ",
                     ifelse(is.na(res[[currentLanguage]]$translation),
                            res[[currentLanguage]]$english,
                            res[[currentLanguage]]$translation))),
              collapse = "\n");

        }        

      } else if (taskSpecs[[taskName]]$task_type == "country_i18n") {
                
        ufs::cat0("This is an i18n task with different ",
                  "content for each country.\n");
        
        res$countryMapping <- list();
        
        ### Get keys and countries
        res$countryMapping$keys <- taskInput[[taskName]][[dataLanguage]]$id;
        res$countryMapping$countries <-
          strsplit(gsub("\\s", "", taskInput[[taskName]][[dataLanguage]][[3]]),
                   ",",
                   fixed = TRUE);
        
        ### Collapse countries into regular expressions

        ### First add start and end
        res$countryMapping$regexes <-
          lapply(res$countryMapping$countries, function(x) return(paste0("^", x, "$")));
        ### Then collapse 
        res$countryMapping$regexes <-
          lapply(res$countryMapping$regexes, paste, collapse = "|");

        ### Get default
        res$countryMapping$defaultValue <-
          which(unlist(lapply(res$countryMapping$regexes, grepl,"xx00")));
        
        if (length(res$countryMapping$defaultValue) == 0) {
          res$countryMapping$defaultValue <- 1;
        }

        ### Store the id - regex combinations as list and convert to json
        res$countryMapping$mapping <-
          lapply(seq_along(res$countryMapping$keys),
                 function(i) {
                   return(stats::setNames(list(res$countryMapping$keys[[i]],
                                               res$countryMapping$regexes[[i]]),
                                          nm = c("key",
                                                 "regex")));
                 });
        names(res$countryMapping$mapping) <- res$countryMapping$keys;
        res$countryMapping$mapping <-
          res$countryMapping$mapping[!is.na(res$countryMapping$keys)];
        res$countryMapping$mapping <-
          c(res$countryMapping$mapping,
            stats::setNames(
              list(
                stats::setNames(
                  list(
                    res$countryMapping$keys[[res$countryMapping$defaultValue]],
                    ".*"
                  ),
                  nm = c("key",
                         "regex")
                )
              ),
              nm = "else"
            )
          );
        res$countryMapping$json <-
          jsonlite::toJSON(res$countryMapping$mapping);

        for (currentLanguage in names(taskInput[[taskName]])) {

          ufs::cat0("Starting to process language ", currentLanguage, ".\n");
          
          ### Object for this language
          res[[currentLanguage]] <- list();
          
          ### Create dataframe for convenience
          dat <- taskInput[[taskName]][[currentLanguage]];
          
          ### Get keys
          res[[currentLanguage]]$keys <-
            res$countryMapping$keys;
          
          if (length(dat$text_en) == length(res$countryMapping$keys)) {

            ### Get English texts
            res[[currentLanguage]]$english <-
              stats::setNames(dat$text_en,
                              nm = res[[currentLanguage]]$keys);
            
            ### Get translations
            res[[currentLanguage]]$translation <-
              stats::setNames(dat$text_translated,
                              nm = res[[currentLanguage]]$keys);
            
            ### Store merged output
            res[[currentLanguage]]$mergedOutput <-
              res[[currentLanguage]]$output <-
              paste0(
                paste0(
                  res[[currentLanguage]]$keys,
                  "=",
                  gsub(
                    "\n|\r",
                    " ",
                    ifelse(
                      is.na(res[[currentLanguage]]$translation),
                      res[[currentLanguage]]$english,
                      res[[currentLanguage]]$translation)
                  )
                ),
                collapse = "\n");
          } else {
            
            ufs::cat0("  -> Lengths don't match up, skipping this language!\n");
            ufs::cat0("       (Specifically, there are ", length(dat$text_en),
                      " English texts in `text_en`, but ",
                      length(res$countryMapping$keys), " keys specified in ",
                      "`res$countryMapping$keys`: ", ufs::vecTxt(res$countryMapping$keys,
                                                                 useQuote="`"),
                      ")\n");
            res[[currentLanguage]]$mergedOutput <- NA;
          }

        }

      } else if (taskSpecs[[taskName]]$task_type == "hotfixes") {
        
        ufs::cat0("This is an hotfixes task.\n");

        for (currentLanguage in v1Languages) {

          ufs::cat0("Starting to process language ", currentLanguage, ".\n");

          ### Object for this language
          res[[currentLanguage]] <- list();
          
          ### Create dataframe for convenience
          if (currentLanguage %in% names(taskInput[[taskName]])) {
            dat <- taskInput[[taskName]][[currentLanguage]];
          } else {
            dat <- taskInput[[taskName]][["copy this one!"]];
          }

          ### Get keys
          res[[currentLanguage]]$keys <-
            dat$key;
          
          ### Get texts
          if (currentLanguage=="en") {
          res[[currentLanguage]]$translation <-
            stats::setNames(dat$text_en,
                            nm = dat$key);
          } else {
            res[[currentLanguage]]$translation <-
              stats::setNames(dat$text_translated,
                              nm = dat$key);
          }
          
          ### Replace missing values with ""
          res[[currentLanguage]]$translation <-
            ifelse(is.na(res[[currentLanguage]]$translation),
                   "",
                   res[[currentLanguage]]$translation);

          res[[currentLanguage]]$mergedOutput <-
            res[[currentLanguage]]$output <-
            paste0(
              paste0(
                res[[currentLanguage]]$keys,
                "=",
                gsub("\n|\r",
                     " ",
                     res[[currentLanguage]]$translation)),
              collapse = "\n");

        }

      } else {
        stop("For task '", taskName,
             "' the task type has an unknown value: ",
             taskSpecs[[taskName]]$task_type, ".");
      }
      
      ### Return result for this task
      return(res);
    }
  );

names(taskOutput) <-
  translationTasks;

```

```{r merge-i18n-fragments}

###-----------------------------------------------------------------------------
### Merge i18n text fragments
###-----------------------------------------------------------------------------

i18nTasks <-
  unlist(lapply(taskSpecs,
                function(x)
                  return(grepl("i18n|hotfix", x$task_type))
                )
         );
i18nTaskNames <- names(taskSpecs)[i18nTasks];
allLanguages <-
  sort(
    unique(
      unlist(
        lapply(
          i18nTaskNames,
          function(taskName) {
            return(names(taskOutput[[taskName]]));
          })
        )
      )
    );

allLanguages <-
  allLanguages[allLanguages != "countryMapping"];

merged_i18n <-
  lapply(
    allLanguages,
    function(currentLanguage) {
      return(
        paste0(
          unlist(
            lapply(
              i18nTaskNames,
              function(taskName) {
                res <- taskOutput[[taskName]][[currentLanguage]]$mergedOutput;
                if (is.null(res) || is.na(res)) {
                  res <-
                    paste0("\n### No keys found for language `",
                           currentLanguage, "` for task `", taskName, "`\n");
                }
                return(res);
              }
            )
          ),
          collapse="\n\n"
        )
      );
    }
  );

### Add final empty row
merged_i18n <-
  lapply(merged_i18n,
         function(x) return(paste0(x, "\n\n")));

names(merged_i18n) <-
  allLanguages;

```

# Write files 

This code writes the files with output, again with some minimal logging.

## Write json files

```{r write-json, comment="", results="asis"}

if (is.null(activeLanguages_prod)) {
  activeLanguages_prod <- allLanguages;
}

for (type in names(taskOutput)) {
  if ("countryMapping" %in% names(taskOutput[[type]])) {
    ufs::cat0("Writing JSON country mapping file for '", type, "'.\n");
    con <- file(file.path(websitePath, "json", paste0(type, ".json")),
                open = "w",
                encoding="UTF-8");
    writeLines(taskOutput[[type]]$countryMapping$json,
               con = con,
               useBytes=FALSE);
    close(con);
  }
}

languageArray <-
  jsonlite::read_json(file.path(websitePath,
                                "json",
                                "languageNames.json"));

###-----------------------------------------------------------------------------
### Production
###-----------------------------------------------------------------------------

selectedLanguagesForJSON_prod <-
  languageArray[activeLanguages_prod];

### Remove empty elements
selectedLanguagesForJSON_prod <-
  selectedLanguagesForJSON_prod[!is.na(names(selectedLanguagesForJSON_prod))];

### Sort alphabetically
selectedLanguagesForJSON_prod <-
  selectedLanguagesForJSON_prod[
    order(
      unlist(
        lapply(
          selectedLanguagesForJSON_prod,
          function(x) return(x[[1]])
        )
      )
    )
  ];

jsonlite::write_json(selectedLanguagesForJSON_prod,
                     file.path(websitePath,
                               "json",
                               "languages.json"));

###-----------------------------------------------------------------------------
### Staging
###-----------------------------------------------------------------------------

selectedLanguagesForJSON_stge<-
  languageArray[activeLanguages_stge];

### Remove empty elements
selectedLanguagesForJSON_stge <-
  selectedLanguagesForJSON_stge[!is.na(names(selectedLanguagesForJSON_stge))];

### Sort alphabetically
selectedLanguagesForJSON_stge <-
  selectedLanguagesForJSON_stge[
    order(
      unlist(
        lapply(
          selectedLanguagesForJSON_stge,
          function(x) return(x[[1]])
        )
      )
    )
  ];

jsonlite::write_json(selectedLanguagesForJSON_stge,
                     file.path(websitePath,
                               "json",
                               "staging-languages.json"));

###-----------------------------------------------------------------------------
### Testing
###-----------------------------------------------------------------------------

selectedLanguagesForJSON_test <-
  languageArray[activeLanguages_test];

### Remove empty elements
selectedLanguagesForJSON_test <-
  selectedLanguagesForJSON_test[!is.na(names(selectedLanguagesForJSON_test))];

### Sort alphabetically
selectedLanguagesForJSON_test <-
  selectedLanguagesForJSON_test[
    order(
      unlist(
        lapply(
          selectedLanguagesForJSON_test,
          function(x) return(x[[1]])
        )
      )
    )
  ];

jsonlite::write_json(selectedLanguagesForJSON_test,
                     file.path(websitePath,
                               "json",
                               "testing-languages.json"));

```

## Write merged i18n files

```{r write-files, comment="", results="asis"}

### Delete all old files
filesToDelete <- list.files(i18nOutputPath_prod);
unlink(file.path(i18nOutputPath_prod, filesToDelete));

filesToDelete <- list.files(i18nOutputPath_stge);
unlink(file.path(i18nOutputPath_stge, filesToDelete));

filesToDelete <- list.files(i18nOutputPath_test);
unlink(file.path(i18nOutputPath_test, filesToDelete));

filesToDelete <- list.files(translationOutputPath);
unlink(file.path(translationOutputPath, filesToDelete));

### Using
### https://kevinushey.github.io/blog/2018/02/21/string-encoding-and-r/

merged_i18n_utf8 <-
  stats::setNames(lapply(merged_i18n, enc2utf8), #utf8::as_utf8),
                  nm = allLanguages);

for (currentLanguage in allLanguages) {

  currentFilenamePre <-
    file.path(translationOutputPath,
              paste0("UTF8_i18n_",
                     currentLanguage,
                     ".properties"));
  currentFilenamePost <- paste0(currentFilenamePre,
                                ".output");
  currentFilenameFinal <- sub("/UTF8_",
                              "/",
                              sub("\\.output$", "", currentFilenamePost));

  ### Using
  ### https://kevinushey.github.io/blog/2018/02/21/string-encoding-and-r/

  con <- file(currentFilenamePre,
              open = "w",
              encoding="native.enc"); #"UTF-8");
  writeLines(merged_i18n_utf8[[currentLanguage]], con = con, useBytes=TRUE);
  close(con);

  shellCommand <-
    paste0(magicToolCommand,
           currentFilenamePre);

  shellResult <- shell(shellCommand, intern=TRUE);
  
  ufs::cat0("\nResult of calling Joao's magic tool with '", shellCommand,
            "': '",
            shellResult, "'.\n");
  
  if (file.exists(currentFilenamePost)) {
    if (file.exists(currentFilenameFinal)) {
      unlink(currentFilenameFinal);
    }
    file.rename(from = currentFilenamePost,
                to = currentFilenameFinal);
  }

  ufs::cat0("\nWrote file '", basename(currentFilenamePre),
            "' and converted it with Joao's magic tool to '",
            basename(currentFilenameFinal), "'.  \n");

  if (currentLanguage %in% activeLanguages_test) {
  
    file.copy(currentFilenameFinal,
              i18nOutputPath_test,
              overwrite = TRUE);
  
    ufs::cat0("Copied file '", basename(currentFilenameFinal),
              "' to testing i18n directory '",
              i18nOutputPath_test, "'.  \n");
  
  }
  
  if (currentLanguage %in% activeLanguages_stge) {
  
    file.copy(currentFilenameFinal,
              i18nOutputPath_stge,
              overwrite = TRUE);
  
    ufs::cat0("Copied file '", basename(currentFilenameFinal),
              "' to staging i18n directory '",
              i18nOutputPath_stge, "'.  \n");
  
  }
  
  if (currentLanguage %in% activeLanguages_prod) {
    
    file.copy(currentFilenameFinal,
              i18nOutputPath_prod,
              overwrite = TRUE);

    ufs::cat0("This language is an active production language; ",
              "copied file '", basename(currentFilenameFinal),
              "' to production i18n directory '",
              i18nOutputPath_prod, "'.  \n");
    
  }
  
  ###---------------------------------------------------------------------------
  ### Rename i18n file if this is the primary language for production or testing
  ###---------------------------------------------------------------------------
  
  if (currentLanguage==primaryLanguage_prod) {

    primaryLang_i18n_filename_prod <-
      sub(paste0("_", currentLanguage),
          "",
          basename(currentFilenameFinal),
          fixed=TRUE);
    
    file.rename(file.path(i18nOutputPath_prod,
                          basename(currentFilenameFinal)),
                file.path(i18nOutputPath_prod,
                          primaryLang_i18n_filename_prod));
                          
    ufs::cat0("This language is the primary language for production; ",
              "renamed its i18n file to '",
              primaryLang_i18n_filename_prod,
              "'.  \n");

  }
  
  if (currentLanguage==primaryLanguage_test) {

    primaryLang_i18n_filename_test <-
      sub(paste0("_", currentLanguage),
          "",
          basename(currentFilenameFinal),
          fixed=TRUE);
    
    file.rename(file.path(i18nOutputPath_test,
                          basename(currentFilenameFinal)),
                file.path(i18nOutputPath_test,
                          primaryLang_i18n_filename_test));
                          
    ufs::cat0("This language is the primary language for testing; ",
              "renamed its i18n file to '",
              primaryLang_i18n_filename_test,
              "'.  \n");
    
  }
  
  if (currentLanguage==primaryLanguage_stge) {

    primaryLang_i18n_filename_stge <-
      sub(paste0("_", currentLanguage),
          "",
          basename(currentFilenameFinal),
          fixed=TRUE);
    
    file.rename(file.path(i18nOutputPath_stge,
                          basename(currentFilenameFinal)),
                file.path(i18nOutputPath_stge,
                          primaryLang_i18n_filename_stge));
                          
    ufs::cat0("This language is the primary language for staging; ",
              "renamed its i18n file to '",
              primaryLang_i18n_filename_stge,
              "'.  \n");
    
  }

  cat("\n");

}

```

# Configuration

## Read and process email addresses for each country

```{r email-addresses-per-country}

countryEmailAddresses <-
  ifelse(
    is.na(taskInput$config$en$alias),
    taskInput$config$en$email,
    taskInput$config$en$alias
  );
countryEmailAddresses <-
  c(
    paste0(
      ifelse(
        taskInput$config$en$email_created,
        countryEmailAddresses,
        "info"
      ),
      "@your-covid-19-risk.com"
    ),
    "info@your-covid-19-risk.com"
  );

countryEmailRegexes <-
  c(taskInput$config$en$locale_regex,
    ".*");

countryEmailKeys <-
  c(taskInput$config$en$email,
    "else");

jason$country_emails <-
  lapply(seq_along(countryEmailKeys),
         function(i) {
           return(list(regex = countryEmailRegexes[i],
                       email = countryEmailAddresses[i]))
         });
names(jason$country_emails) <-
  countryEmailKeys;

```

## Write jason to `your-covid-19-risk-config.json`

```{r write-jason, comment="", results="asis"}

###-----------------------------------------------------------------------------
### Production version
###-----------------------------------------------------------------------------

### Write production version
jsonlite::write_json(jason,
                     file.path(websitePath,
                               "json",
                               "your-covid-19-risk-config.json"));

###-----------------------------------------------------------------------------
### Staging version
###-----------------------------------------------------------------------------

### Create staging version
jason_stge             <- jason;
jason_stge$tool_url    <- toolURL_stge;
jason_stge$primary_language <- primaryLanguage_stge;

### Write testing version
jsonlite::write_json(jason_stge,
                     file.path(websitePath,
                               "json",
                               "your-covid-19-risk-stge-config.json"));

###-----------------------------------------------------------------------------
### Testing version
###-----------------------------------------------------------------------------

### Create testing version
jason_test             <- jason;
jason_test$tool_url    <- toolURL_test;
jason_test$primary_language <- primaryLanguage_test;

### Write testing version
jsonlite::write_json(jason_test,
                     file.path(websitePath,
                               "json",
                               "your-covid-19-risk-test-config.json"));

```

# Imported sheets {.tabset}

## Imported sheets

See the tabs for the imported sheets per task.

```{r show-input, comment="", results="asis", eval=FALSE}

for (taskName in names(taskOutput)) {
  ufs::cat0("\n\n\n\n## ", taskName, " {.tabset}\n\n\n\n");
  for (currentLanguage in names(taskOutput[[taskName]])) {
    ufs::cat0("\n\n\n\n### ", currentLanguage, "\n\n\n\n");
    print(
      kableExtra::kable_styling(
        knitr::kable(taskInput[[taskName]][[currentLanguage]],
                     escape = FALSE)
      )
    );
  }
}

```

# Output by task {.tabset}

## Output by task first, language second

Click the tabs to see the output for each task, and then the tabs below those to select the language.

```{r show-by-task, comment="", results="asis", eval=FALSE}

for (taskName in names(taskOutput)) {
  ufs::cat0("\n\n\n\n## ", taskName, " {.tabset}\n\n\n\n");
  for (currentLanguage in names(taskOutput[[taskName]])) {
    ufs::cat0("\n\n\n\n### ", currentLanguage, "\n\n\n\n");
    ufs::cat0("\n\n\n<pre>\n",
              taskOutput[[taskName]][[currentLanguage]]$mergedOutput,
              "\n</pre>\n\n\n");
  }
}

```

# Output by language {.tabset}

## Output by language first, task second

Click the tabs to see the output in each language, and then the tabs below those to select the task.

```{r show-by-language, comment="", results="asis", eval=FALSE}

### Get all languages
allLanguages <-
  sort(
    unique(
      unlist(
        lapply(
          taskOutput,
            function(x) {
              return(names(x));
            })
        )
      )
    );

for (currentLanguage in allLanguages) {
  ufs::cat0("\n\n\n\n## ", currentLanguage, " {.tabset}\n\n\n\n");
  for (taskName in names(taskOutput)) {
    ufs::cat0("\n\n\n\n### ", taskName, "\n\n\n\n");
    if (currentLanguage %in% names(taskOutput[[taskName]])) {
      ufs::cat0("\n\n\n<pre>\n",
                taskOutput[[taskName]][[currentLanguage]]$mergedOutput,
                "\n</pre>\n\n\n");
    } else {
      ufs::cat0("\n\n\nCould not find language '", currentLanguage,
                "' in task '", taskName, "'.\n\n\n");
    }
  }
}

```
