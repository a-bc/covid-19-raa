<script>
  if (document.location.pathname.endsWith("theory.html")) {
    document.location.href="/#theory";
  }
</script>
<!-- Masthead-->
<header class="masthead shortmasthead">
  <div class="container h-25">
    <div class="col-lg-10 align-self-end">
      <h1 class="text-uppercase text-white font-weight-bold" data-i18n="[html]theory_main_header">Theory</h1>
    </div>
  </div>
  </div>
</header>
<!-- Theory section-->
<section class="page-section" id="theory">
  <div class="container text-white">
    <h2 class="text-center text-primary mt-0" data-i18n="[html]home_head_theory">The theory</h2>
    <hr class="divider my-4"/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_background_heading">Background</h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_background_text1">Disclaimer: this page explains what makes this project possible, and as such, discusses the underlying principles and systems. This text is meant for those with an interest in what is going on under the hood, and because what is going on under the hood concerns health psychology and technology, that means that this text was not primarily written to be accessible to a wide audience; that would have required including large chunks of psychology bachelor programs and technology tutorials. As a consequence, this text may not be easy to read for readers lacking that background - apologies for that in advance.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_background_text2">This tool was created in six weeks, in a worldwide, largely decentralized collaboration of over a hundred experts who contributed their time, energy, and expertise. Realising this would not have been possible without an extremely rigorous, systematic, and scalable approach, which manifested itself in the project’s infrastructure.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_background_text3">This infrastructure has three layers. From abstract to tangible, these are the theoretical layer, the methodological layer, and the operational layer. The operational layer leverages a number of technologies to enable operationalization of the methodological implementation of the theoretical layer. The most central technologies were Slack, for communication; Google Sheets spreadsheets for systematically collecting information and translations; Google Docs text processor documents for presenting instructions; LimeSurvey, for building the tool itself; R for processing the spreadsheets and producing the survey file to import into LimeSurvey and the website; the i18next javascript library for internationalization, and GitLab for collaboration and version control. All files used in this project are openly available at our <a href="https://your-risk.com/git" target="_blank">GitLab repository</a>. This document does not describe the operational implementation of this project (in fact, as yet, that still has to be documented).</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_background_text4">The methodological layer and the theoretical layer are closely intertwined, as is common in psychology. Before explaining those layers, however, it is important to make something clear.</div>
      </div>
    </div>
    <br/>
    <br/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_noscience_heading">No scientific study</h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_noscience_text1">This project is not a scientific endeavour. Instead, this project leverages knowledge obtained in psychological science to address a real-world problem. The project does not aim to test psychological theory, and as such, it does not aim to advance our scientific knowledge. In that sense, this project is more like building a car, instead of studying the principles of combustion. However, just like building a car to address the real-world problem of mobility requires knowledge of physics, building a tool to address the real-world problem of COVID-19-related behavior requires knowledge of psychology. Because this text will most likely be of interest to (and therefore, be read by) researchers, this is important to clarify. This project is not basic (or 'pure') science, and not even applied science, since the aim is not to learn about the universe. This project applies knowledge gained through the scientific method, but the project itself is not scientific, just like building a car, a bridge, or a rocket is not science. However, that does not mean we can disregard what we know about how human psychology works (just like it would be unwise to disregard the laws of physics when building a car or a bridge). It also does not mean it is impossible to learn anything; after all, all human endeavours (and many other things) inevitably produce data, and if those data are systematically registered, stored, and made available, who knows what insights it might yield? However, those data are in themselves the product of a scientific endeavour in the same way the sales data of a webshops are: not at all. A webshop may even experiment with different background colors and systematically monitor the effects on their sales figures, and this can provide useful, actionable insights to them; but that does not make the exercise a scientific study.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_noscience_text2">In this project, we do not have research questions or hypotheses. We do not test any theory. Instead, we use theory (and already existing empirical results) to address a real-world problem.</div>
      </div>
    </div>
    <br/>
    <br/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_thetool_heading">The tool</h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_thetool_text1">That real-world problem is the existence of SARS-CoV-2 (commonly known as "the coronavirus"), the damage caused by COVID-19 (the disease caused by SARS-CoV-2), and the damage caused by the measures that are required to contain the coronavirus. Most volunteer experts in this project are health psychologists, and most of them are specialized in behavior change, and this is mostly how this project aims to help address this real-world problem: by helping people to change their behavior to ultimately contain the coronavirus. There are two routes to achieve this aim: by directly supporting people, and by providing governments and health agencies with support when developing behavior change interventions.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_thetool_text2">The first route is realised through the Risk Estimate and the Safety Estimate that tool users receive. Both are optimized for the tool users' situations based on the questions the tool users answer in the tool. The Risk Estimate is discussed more in detail on the Risk Model page. The Safety Estimate was developed using Acyclic Behavior Change Diagrams (ABCDs), a tool that facilitates making the causal and structural assumptions underlying an intervention explicit. When working with ABCDs, one first constructs an ABCD matrix, a matrix with seven columns, each of which represents one crucial link in the causal-structural chain responsible for potential intervention effectiveness.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_thetool_text3">The second route is realised through recommendations for governments and health agencies. In these recommendations, we will support governments and health agencies in their communication with the residents of their countries to target the most important sub-determinants of behavior. Specifically, in the first version of the tool, we will ask the tool users some questions to improve our tool. However, the information we need to improve the tool is exactly the same information that governments and health agencies need to be able to develop effective interventions. This second route will ultimately have more potential than the first route, because this project has a number of constraints that cap the potential effectiveness of the first route. Specifically, most tool users will use mobile phones to access the tool. This means that we only have very small screens available, often no audio, and only have tool users' attention for a brief time. In addition, all work in this project has been done by volunteers, using freely available or donated software and architecture. Governments and health agencies have considerable funds available for intervention development, and so can potentially obtain large effects, but only if they know what to target. That is exactly what the second route will provide.</div>
      </div>
    </div>
    <br/>
    <br/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_safetyestimate_heading"></h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text1">The Safety Estimate was developed using Acyclic Behavior Change Diagrams (ABCDs) to optimize alignment between the aspects of human psychology that it targets and the behavior change principles that were used, as well as the adequate application of those behavior change principles. This alignment ensures that the tool offers users, as much as possible, what is needed for behavior change in terms of presented stimuli (i.e. text, images, video, etc).</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text2">The alignment is based on a causal-structural chain comprising seven links. The chain ends with the target behavior (ideally, the enactment of the target behavior). Such target behaviors are typically relatively broadly defined, such as “social distancing”, “self-isolation”, or “hand-washing”. Each of these behaviors consists of sub-behaviors; for example, hand-washing can be considered to consist of using soap, washing one’s hand for sufficiently long, and using the right technique (other ‘divisions’ into sub-behaviors are also possible, of course).</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text3">Each target behavior is enacted as a consequence of an individual’s environment and their psychology. ABCDs deal with the latter of these two, and use psychological constructs to describe people’s psychology. Because these constructs theoretically determine an individual’s behavior they are called determinants, and they can be defined very generally (such as “attitude”) or very specifically (such as “perceived probability that using soap will kill the coronavirus”; Peters & Crutzen, 2017). Although most psychological research occurs at the general level, when communicating with people, you need to know what to say on a very specific level (Peters, 2014). In ABCDs, two levels of constructs are distinguished: determinants are at the general level, and sub-determinants are very specific.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text4">Before communicating with people about any behavior, therefore, it is necessary to know which sub-determinants to communicate about. If somebody wants to get infected with the coronavirus, they will have a positive attitude towards getting infected. Communicating with that person effectively requires knowing <em>why</em> they have a positive attitude. This may be, for example, because the intensive care units where that person lives have enough capacity, but that person is afraid they will become overwhelmed, so it is better to get sick now that treatment is still possible. It may also be because that person believes that they will not get very sick, because they are young, and that they can go back to work faster once they are infected. It may also be that they are afraid that the disease will evolve to become deadlier, but that immunity, once acquired, will also protect from those more lethal variations of the coronavirus.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text5">Because effective communication requires knowing the relevant sub-determinants, those are perhaps the most important link in each causal-structural chain. The sub-determinants, which together form the determinants of each sub-behavior, are what health communications need to address. However, the first three links are very important as well. They concern the methods used to address those sub-determinants. Any health communication that has an effect on the target behavior achieves this through learning. People who are exposed to the intervention learn something (Crutzen & Peters, 2018). Learning means that something changes in a person’s psychology, and constructs are what we use to describe parts of people’s psychology. In health communication, therefore, the aim is to identify the parts of people’s psychology where there is potential for learning, such that those people are supported in their behavior change.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text6">But learning is not trivial. Learning is an ability organisms such as humans acquired through evolution, but those evolutionary learning processes never evolved to facilitate health communication. To successfully address (sub-)determinants, therefore, requires correct application of the behavior change principles (BCPs) that leverage the nine evolutionary learning processes through which humans can learn (Crutzen & Peters, 2018). These BCPs have been studied in psychological science for over a hundred years, and long lists of such behavior change principles exists (e.g. Bartholomew et al., 2016). Like determinants, those BCPs are defined at a general, abstract level, and to use them in health communication first requires applying them correctly. To do so requires adhering to each BCPs conditions for effectiveness; violating those means that the underlying evolutionary learning processes are no longer effectively engaged. The first three links for the causal-structural chain, therefore, are the BCP, the corresponding conditions of effectiveness, and the application in which the BCP is applied. One application can contain many BCPs (Kok, 2014), and the same BCP can also be applied in different ways, depending on what works best for the intended audience of the health communication.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text7">The full causal-structural chain that underlies health communications’ effectiveness, then, is as follows:</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text8">
          <ol>
            <li>An effective behavior change principle (BCP) is used, and</li>
            <li>according to the corresponding conditions for effectiveness,</li>
            <li>that BCP is applied into an application, which targets</li>
            <li>an important and sufficiently specific sub-determinant, which is a part of</li>
            <li>a generally defined determinant that determines (together with other determinants)</li>
            <li>a sub-behavior, which is a specifically defined behavior that forms (together with other sub-behaviors)</li>
            <li>the target behavior</li>
          </ol>
        </div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text9">Acyclic Behavior Change Diagram matrices are spreadsheets where every row is one such causal-structural chain. Together, therefore, an ABCD matrix expresses many assumptions underlying the expected effectiveness of any health communication, intervention, or campaign. These ABCD matrices can be parsed and used to produce the actual Acyclic Behavior Change Diagrams, which are visual representations of these causal-structural chains. The Acyclic Behavior Change Diagrams for the Safety Estimate are available at <a href="https://your-risk.com/v1-abcds" target="_blank">https://your-risk.com/v1-abcds</a>.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text10">These ABCDs express exactly which (sub-)determinants the Safety Estimate addresses, and which Behavior Change Principles are implemented in each message shown to tool users. This systematic approach makes it possible for the tool to be improved over time based on new insights about important (sub-)determinants: in other words, as we learn more about why people do what they do in terms of coronavirus-related behaviors, we are able to improve the tool to meet people’s needs better.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_safetyestimate_text11">To enable these improvements, the tool contains another important part: the Determinant Mapping Questions.</div>
      </div>
    </div>
    <br/>
    <br/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_dmqs_heading">The Determinant Mapping Questions</h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text1">Because the upper bound of intervention effectiveness is formed by how much of the relevant parts of people’s psychology is addressed by that health communication, it is vital to know the important (sub-)determinants of behavior. Improving our tool over time, therefore, requires acquiring information about those sub-determinants. Therefore, we ask our tool users whether they are willing to help us improve the tool by answering a few questions. This is similar to customer satisfaction surveys used routinely by companies, with three important differences.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text2">First, we use the results to improve a free service instead of a commercial service. Second, our goal (i.e. improving health communications) comes with very specific constraints as to how we have to ask the questions. Third, because we do this project for the greater good, we will make all answers of our tool users, which are provided anonymously, available in the public domain. This means that this same information can be used by governments and health agencies that want to communicate effectively with people about coronavirus-related behaviors. For more information about our data sharing policy, see <a href="#data">this page</a>; for now, we will go into the kinds of questions we ask our tool users.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text3">Psychological research has yielded a wealth of information about explaining behavior with psychological constructs, as well as about how to successfully change those psychological constructs (i.e. engage underlying learning processes). Those constructs have specific definitions and specific instructions for developing measurement instruments. For example, one such construct is called “instrumental attitude belief expectation”, and it refers to people’s expectation of the consequences of a behavior as they relate to that person’s goals. For example, if somebody says “When I wash my hands, it doesn’t matter whether I use soap, because I make sure I clean very thoroughly anyway!”, that expression conveys information about the expectation that person has of the consequences of a behavior: using soap. Another example is “When I use soap, my hands get all dry”, or “Soap effectively kills the coronavirus”.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text4">Note that it is not always obvious how these expressions relate to the construct “instrumental attitude belief expectation”, which is defined as the expectation that a given consequence will or will not occur. The last one, for example (“Soap effectively kills the coronavirus”) only describes the aspect of people’s psychology covered by the construct “instrumental attitude belief expectation” if it is changed a bit by introducing the link to the target behavior: “If I use soap when I wash my hands, that will kill the coronavirus”. This is important because people might believe, for example, that although soap in general kills the coronavirus, it won’t work when they wash their hands, or it won’t work for them specifically.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text5">This level of specificity and accuracy is required to be able to think and communicate about aspects of human psychology, or, more accurately, about those aspects captured by a given construct. It is excessively hard to develop definitions that are sufficiently clear and specific to allow unequivocal communication and consistent measurement (and change), but this becomes slightly easier because psychological theories (like any theories) have very limited scopes. For example, the Reasoned Action Approach only deals with explaining <em>reasoned</em> action. If a behavior is largely habitual, that theory is of little use, and you are better off using theories specialized in automatic behaviors, instead. These specific scopes allow theories to provide sufficiently specific definitions of the constructs they use.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text6">An inevitable consequence of this required specificity is that developing measurement instruments for these constructs is a matter of extreme precision. Unfortunately, when published, psychological theories tend to neglect the responsibility this creates. It is rare that theories provide very extensive, clear, accurate definitions of their constructs, delineating as well as possible which aspects of human psychology are a part of each construct, and which are not. Similarly (perhaps as a necessary consequence), clear instructions for operationalization (e.g. developing measurement instruments) often lack. As a consequence, measurement in psychology is somewhat of a mess (see e.g. <a href="https://docs.google.com/document/d/1vaq0U3_U2PEgouPLteUjab15aVxyEv2IVdbOxTErkHM" target="">this crowd-sourced list of instruments measuring the same constructs (which turned out not to do so))</a>.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text7">This is especially problematic because psychological constructs do not exist as more or less discrete, modular entities in the human mind (Peters & Crutzen, 2017). This has two important implications.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text8">First, there are no “correct definitions” of a construct. This means that variability in definitions of constructs is in itself not a problem. Quite the opposite: that variability is important and facilitates progress, because it avoids capitalization on single (and wrong) definitions, instead allowing multiple definitions to be used in parallel and studying how they relate to each other. Rather, the problem is the present inability to unequivocally communicate about constructs.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text9">Second, if measurement instruments use different stimuli, they may measure different constructs. This is somewhat counter to the way measurement instruments of psychological constructs are normally treated: as if different questionnaires of a psychological construct measure the same construct, and can be meaningfully aggregated. This is unfortunately not the case, but improving this first requires a way to unequivocally communicate about different variations of constructs, distinguished by different definitions and different (but internally consistent) instructions for operationalization. As long as constructs are defined only at a very vague level and always referred to using natural language (e.g.”depression”), it is easy for different researchers to retain different definitions, and think different measurement instruments should be used, while at the surface appearing to be discussing the same construct.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text10">Fixing these problems requires means to achieve unequivocal communication about construct definitions and guidelines for operationalization, in a way that does not require central curation. This decentralization is important because these constructs do not exist as discrete entities, and the variability in definitions is valuable. The variability is not the problem: the inability to accurately, unequivocally communicate is the problem.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text11">To be able to improve our tool over time, we need to consistently ask the same questions; we need to know which sub-determinants we map. This project therefore requires exactly this solution: we need to be able to work with clear construct definitions and corresponding instructions for developing measurement instruments.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text12">The technology we use are the Decentralized Construct Taxonomies (perhaps it can most accurately be described as ‘theoretical technology’, as it is mainly a tool for working with psychological theory, or ‘methodological technology’ as it facilitates operationalization). Decentralized Construct Taxonomies (DCTs) allow specifying exact construct definitions and corresponding instructions for operationalisation and attaching a unique construct identifier (UCID). This enables accurate operationalization of those constructs, consistent with that definition, and the UCID enables unequivocal reference to that exact definition and those exact instructions for developing measurement instruments.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text13">Those UCIDs can be produced using the R package psyverse, which allows anybody to generate UCIDS. DCTs can easily be shared and re-used, but they can also easily be updated (and assigned new UCIDs), and new ones can easily be created. This allows decentralization: everybody can work with DCTs without the need for central oversight. It simultaneously achieves unequivocality: every UCID is unique, so by using those UCIDs and publishing the relevant DCTs, it is always clear exactly what is being referred to.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text14">These DCTs can then be applied to specific aspects of human psychology to create, if at all possible, measurement instruments that capture that bit of human psychology. In this project, that means that using DCTs enables us to create questions that all fit with the relevant construct’s definition as specified in the DCT specification accompanying the instructions for developing measurement instruments that were applied for each question. For each different construct, different BCPs fit better, so using DCTs allows us to select the best BCPs for each sub-determinant in our improved Safety Estimate. In addition, we can share our insights with other organisations, such as governments and health agencies. They can use these to improve their own health communications.</div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_dmqs_text15">The theory we use in the first version of this tool is the Reasoned Action Approach (Fishbein & Ajzen, 2010). The definitions of the twelve constructs we use are listed in our DCT specifications, which are available in their raw form in our git repository at <a href="https://your-risk.com/git" target="_blank">https://your-risk.com/git</a> and in a more friendly form at <a href="https://your-risk.com/v1-dct-specs" target="_blank">https://your-risk.com/v1-dct-specs</a>. The psyverse R package is freely available from CRAN, and its website is located at <a href="https://r-packages.gitlab.io/psyverse/" target="_blank">https://r-packages.gitlab.io/psyverse/</a></div>
      </div>
    </div>
    <br/>
    <br/>
    <div class="row">
      <h4 class="col-lg-12 text-center text-white mt-2" data-i18n="[html]theory_references_heading">References</h4>
      <br/>
    </div>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_references_text1">Kok, G., Gottlieb, N. H., Peters, G.-J. Y., Mullen, P. D., Parcel, G. S., Ruiter, R. A. C., Fernández, M. E., Markham, C., & Bartholomew, L. K. (2016). A taxonomy of behavior change methods: An Intervention Mapping approach. Health Psychology Review, 10(3), 297–312. <a href="https://doi.org/10.1080/17437199.2015.1077155" target="_blank">https://doi.org/10.1080/17437199.2015.1077155</a></div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_references_text2">Crutzen, R., & Peters, G.-J. Y. (2018). Evolutionary learning processes as the foundation for behaviour change. Health Psychology Review, 12(1), 43–57. <a href="https://doi.org/10.1080/17437199.2017.1362569" target="_blank">https://doi.org/10.1080/17437199.2017.1362569</a></div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_references_text3">Kok, G. (2014). A practical guide to effective behavior change: How to apply theory- and evidence-based behavior change methods in an intervention. European Health Psychologist, 16(5), 156–170. <a href="https://doi.org/10.31234/osf.io/r78wh" target="_blank">https://doi.org/10.31234/osf.io/r78wh</a></div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_references_text4">Peters, G.-J. Y. (2014). A practical guide to effective behavior change: How to identify what to change in the first place. European Health Psychologist, 16(5), 142–155. <a href="https://doi.org/10.31234/osf.io/hy7mj" target="_blank">https://doi.org/10.31234/osf.io/hy7mj</a></div>
      </div>
    </div>
    <br/>
    <div class="row">
      <div class="col-lg-12 col-md-12">
        <div data-i18n="[html]theory_references_text5">Peters, G.-J. Y., & Crutzen, R. (2017). Pragmatic nihilism: How a Theory of Nothing can help health psychology progress. Health Psychology Review, 11(2). <a href="https://doi.org/10.1080/17437199.2017.1284015" target="_blank">https://doi.org/10.1080/17437199.2017.1284015</a></div>
      </div>
    </div>
  </div>
</section>