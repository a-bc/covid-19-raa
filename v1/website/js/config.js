window.globalConfig = {};
$.get({
  headers: {"Content-Type": "application/json; charset=UTF-8"},
  url: "./json/your-covid-19-risk-config.json",
  async: false
}, function (data, status) {
  if (data) {
    $.extend(window.globalConfig, data)
  }
});