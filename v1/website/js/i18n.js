let supportedLanguages = {};
$.get({
  headers: {"Content-Type": "application/json; charset=UTF-8"},
  url: "./json/languages.json",
  async: false
}, function(data, status) {
  if (data) {
    supportedLanguages = data;
  }
});

const defaultLanguage = globalConfig.primary_language[0];
const languageDetector = new window.i18nextBrowserLanguageDetector();

function loadI18n(lang) {
  var lng;
  if(lang.substr(0, 2) === defaultLanguage) {
    lng = "";
  } else {
    lng = "_"+lang;
  }
  $.get({
    headers: {"Content-Type": "text/plain; charset=UTF-16"},
    url: "./i18n/i18n"+lng+".properties",
    async: false
  }, function(data, status){
    const i18n_default = parseLines(data);
    let objData = {}
    for (let i = 0; i < i18n_default.length; i++){
      objData[i18n_default[i][0]] = (i18n_default[i][1] != undefined) ? i18n_default[i][1] : {}
    };
    i18next.addResourceBundle(lang, "translation", objData, true, true);
  });
}

function i18nLocalize() {
  $("#page-top").localize();
  $("head title").localize();
  $("#cookieinfo").localize();

  $(document.body).attr("direction", i18next.dir());
  if (i18next.dir() === "rtl") {
    $(document.body).attr("style", "unicode-bidi: bidi-override !important; direction: unset !important; text-align: right !important;");
  } else {
    $(document.body).attr("style", "");
  }
  $("html").attr("dir", i18next.dir());

  // deal with RTL/LTR
  // $("#page-top div").each(function() {
  //   $(this).attr("direction", i18next.dir());
  // });

  // pres release link
  if ($("#pressReleaseLink").length  > 0) {
    $("#pressReleaseLink").attr("href", "press/press-release-" + i18next.language + ".pdf");
  }

  // email decode (Homepage)
  if ($("#emailButton").length > 0) {
    let emails = globalConfig.country_emails;
    const emailsListKeys = Object.keys(emails);
    for (let i = 0; i < emailsListKeys.length; i++) {
      const emailKey = emailsListKeys[i];
      if (i18next.language.match(emails[emailKey].regex)) {
        $("#emailButton").attr("href", "mailto:" + emails[emailKey].email);
        $("#emailText").attr("href", "mailto:" + emails[emailKey].email);
        break;
      }
    }
  }

  // Tool Button (Homepage)
  if ($("#toolButton").length > 0) {
    $("#toolButton").attr("onclick", "window.location='" + globalConfig.tool_url + supportedLanguages[i18next.language][2] + "'");
  }

  // Remove upper text for greek when the "removeable-uppercase" is used
  if (i18next.language === "el") {
    $(".removeable-uppercase").removeClass("text-uppercase");
  } else {
    $(".removeable-uppercase").addClass("text-uppercase");
  }
}

function doi18n() {
  i18next.use(languageDetector).init({
    whitelist: Object.keys(supportedLanguages),
    debug: false,
    language: defaultLanguage,
    fallbackLng: defaultLanguage,
    detection: {
      order: ['querystring', 'localStorage', 'navigator', 'htmlTag'],
      lookupQuerystring: 'lang'
    },
    resources: {
      en: {
        translation: {
        }
      }
    }
  }, function(err, t) {
    // initialized and ready to go!

    //load the i18n default and current lang file if different from the default
    loadI18n(defaultLanguage);
    loadI18n(i18next.language);

    //initialize jquery i18n
    jqueryI18next.init(i18next, $, {
      tName: 't', // --> appends $.t = i18next.t
      i18nName: 'i18n', // --> appends $.i18n = i18next
      handleName: 'localize', // --> appends $(selector).localize(opts);
      selectorAttr: 'data-i18n', // selector for translating elements
      targetAttr: 'i18n-target', // data-() attribute to grab target element to translate (if different than itself)
      optionsAttr: 'i18n-options', // data-() attribute that contains options, will load/set if useOptionsAttr = true
      useOptionsAttr: true, // see optionsAttr
      parseDefaultValueFromContent: true // parses default values from content ele.val or ele.text
    });

    $(function(){
      // add language languages
      $('.selectpicker').empty();
      Object.keys(supportedLanguages).forEach( function(langId) {
        $("<option data-content=\"<span class='flag-icon flag-icon-"+ supportedLanguages[langId][1] +"'></span> " + supportedLanguages[langId][0] + "\">" + langId + "</option>")
            .appendTo('.selectpicker');
      });

      // add language listener with default value
      $('.selectpicker').selectpicker("val", i18next.language).on("changed.bs.select",
          function(e, clickedIndex, newValue, oldValue) {
            i18next.changeLanguage(this.value);
            loadI18n(this.value);
            i18nLocalize();
          });
    });
    i18nLocalize();
  });
}
